#pragma once
#include <cstdint>
namespace squares
{

using u32 = std::uint32_t;
using u64 = std::uint64_t;

/*/
 * https://squaresrng.wixsite.com/rand
/*/

enum class Rounds : u32
{
    Two   = 2,  // fastest
    Three = 3,
    Four  = 4,  // safest
};

static constexpr u64 exampleKey[] =
{
    0xc8e4fd154ce32f6d,
    0xfcbd6e154bf53ed9,
    0xea6342c76bf95d47,
    0xfb9e125878fa6cb3,
    0xa1ed294ba7fe8b31,
    0xcf29ba8dc5f1a98d,
    0x815a7d4ed4e3b7f9,
    0x163acbf213f5d867,
    0x674e2d1542f9e6d3,
    0xebc9672872ecf651,
    0xec13a6976ecf14ad,
    0x42c86e3a9de3542b,
    0x5489de2cbce65297,
};
static constexpr u64 defaultKey = exampleKey[12];

inline constexpr u32 uniformU32( u64    const counter
                               , Rounds const rounds    = Rounds::Four
                               , u64    const key       = defaultKey
                               ) noexcept
{
    u64 const m[] =
    {
         counter      * key,
        (counter + 1) * key,
    };
    u64 n = m[0];

    for(u32 round = 0; round < static_cast<u32>(rounds); ++round)
    {
        n = n * n + m[round & 0x1];
        n = (n >> 32) | (n << 32);
    }

    return static_cast<u32>(n);
}
inline constexpr u64 uniformU64( u64    const counter
                               , Rounds const rounds    = Rounds::Four
                               , u64    const key       = defaultKey
                               ) noexcept
{
    u64 const m[] =
    {
         counter      * key,
        (counter + 1) * key,
    };
    u64 t = 0;
    u64 n = m[0];

    u32 const R = static_cast<u32>(rounds);
    for(u32 round = 0; round < R; ++round)
    {
        t = n * n + m[round & 0x1];
        n = (t >> 32) | (t << 32);
    }

    return t ^ ((n * n + m[R & 0x1]) >> 32);
}

} // namespace squares
