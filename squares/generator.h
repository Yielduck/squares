#pragma once
#include "squares.h"
namespace squares
{

template<Rounds rounds = Rounds::Four, u64 key = defaultKey>
struct Generator
{
    u64 counter = 0;

    constexpr Generator() = default;
    constexpr Generator(u64 const n) noexcept
        : counter(n)
    {}

    using result_type = u32;
    static constexpr result_type min() noexcept {return 0;}
    static constexpr result_type max() noexcept {return UINT32_MAX;}

    constexpr result_type operator()() noexcept
    {
        return uniformU32(counter++, rounds, key);
    }

    constexpr Generator<rounds, key> split() noexcept
    {
        u64 const lower = this->operator();
        u64 const upper = this->operator();
        return {(upper << 32) | lower};
    }
};

template<Rounds rounds = Rounds::Four, u64 key = defaultKey>
struct Generator64
{
    u64 counter = 0;

    constexpr Generator64() = default;
    constexpr Generator64(u64 const n) noexcept
        : counter(n)
    {}

    using result_type = u64;
    static constexpr result_type min() noexcept {return 0;}
    static constexpr result_type max() noexcept {return UINT64_MAX;}

    constexpr result_type operator()() noexcept
    {
        return uniformU64(counter++, rounds, key);
    }

    constexpr Generator64<rounds, key> split() noexcept
    {
        return {this->operator()};
    }
};

} // namespace squares
