#include <squares/generator.h>
#include <iostream>

int main()
{
    squares::Generator64<squares::Rounds::Four> generator;
    while(true)
    {
        auto const number = generator();
        std::cout.write(reinterpret_cast<char const *>(&number), sizeof(number));
    }
}
